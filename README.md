# Poetry Docker Image

A Poetry Docker image that ensures reproducibility, and hash checking for Poetry and its dependencies, to be used to build other images.

## Usage

Use this image to build another image, where you require access to Poetry (usually in the build stage in multi-stage images):

`FROM registry.gitlab.com/nevrona/public/poetry-docker:<poetry version>-<python version><python flavor>`

Where:

* `poetry version` is desired poetry version, such as `1.2.2`.
* `python version` is desired python version, such as `3.11`.
* `python flavor` is desired python flavor, such as `pypy`, `stackless`, or nothing for CPython.

Example: `FROM registry.gitlab.com/nevrona/public/poetry-docker:1.2.2-3.11`

Check all built images [in our registry](https://gitlab.com/nevrona/public/poetry-docker/container_registry/859361).

### Versions

This project will publish versions equal to Poetry versions, so it's easier to use. So for Poetry v1.2.2, use `registry.gitlab.com/nevrona/public/poetry-docker:1.2.2-<flavor>`, where `flavor` is the desired Python version, and/or interpreter, i.e.:

* Poetry v1.2.2 in CPython 3.10, Debian slim: `registry.gitlab.com/nevrona/public/poetry-docker:1.2.2-3.10`
* Poetry v1.1.12 in PyPy 3.7, Debian slim: `registry.gitlab.com/nevrona/public/poetry-docker:1.1.12-3.7pypy`

**Important**: since v0.3 the base image is no longer Alpine but Debian (slim), unless otherwise specified.

## Building

If you want to build these images yourself, here's the step by step:

1. Clone this repo: `git clone git@gitlab.com:nevrona/public/poetry-docker.git`
2. Use `invoke` to run the build task: `inv build-docker <poetry version> <python version> [--flavor <python flavor>]`
    * Where flavor may be `cpython` (default), `pypy`, or `stackless`.
    * Use `inv -h build-docker` for more information.

Without using `invoke`, do the following instead:

1. Create the corresponding dockerfile by concatenating its parts, i.e.: `cat 0-base.Dockerfile 1-debian.Dockerfile 2-standard.Dockerfile 3-final.Dockerfile > Dockerfile`
    * For `cpython`, or `pypy`, use those.
    * For `stackless`, add `2-stackless.Dockerfile` after `1-debian.Dockerfile`.
    * You can change Debian for any other base creating another dockerfile instead of `1-debian.Dockerfile`.
2. Run docker build command: `sudo docker build --compress --pull --rm --tag registry.gitlab.com/nevrona/public/poetry-docker:<poetry version>-<python version><python flavor> --build-arg POETRY_VERSION=<poetry version> --build-arg BASE=<docker base> .`
    * Choose any corresponding docker base, i.e.:
        * `cpython@debian`: `python:<python version>-slim`
        * `pypy@debian`: `pypy:<python version>-slim`
        * `stackless@debian`: `bcroq/stackless:3.7.8-slim-buster`, or `bcroq/stackless:3.8.1-slim-bullseye`

If you are publishing your docker image, **please adapt `0-base.Dockerfile` accordingly**.

## License

*Poetry docker* is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

**Neither the name of Nevrona S. A. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.**
