"""Common tasks for Invoke."""
from glob import glob
from string import Template

from invoke import Context
from invoke import Exit
from invoke import task

BASE_TAG = "registry.gitlab.com/nevrona/public/poetry-docker"


@task
def lint_docker(ctx):
    """Lint Dockerfile."""
    ctx.run(
        "sudo docker run --rm -i hadolint/hadolint < Dockerfile",
        echo=True,
        pty=True,
        echo_stdin=False,
    )


@task(lint_docker)
def lint(ctx):
    """Lint code and static analysis."""


def get_docker_tag(*, base_tag: str, flavor: str, poetry_version: str, python_version: str) -> str:
    flavor_tag = "" if flavor == "cpython" else flavor
    # Remove the dash from versions such as release candidates
    tag = (
        f"{base_tag}:{poetry_version.replace('-', '')}-"
        + f"{python_version.replace('-rc', '-pre')}{flavor_tag}"
    )

    return tag


@task(
    help={
        "version": "desired poetry version",
        "python_version": "desired python version",
        "flavor": "desired python flavor, one of: cpython (default), pypy, stackless",
        "tag": f"docker image tag (defaults to `{BASE_TAG}`)",
    },
)
def docker_tag(
    _: Context,
    version: str,
    python_version: str,
    flavor: str,
    tag: str = BASE_TAG,
) -> None:
    """Get the docker tag for given parameters."""
    print(
        get_docker_tag(
            base_tag=tag,
            flavor=flavor,
            poetry_version=version,
            python_version=python_version,
        ),
    )


@task(
    help={
        "version": "desired poetry version",
        "python_version": "desired python version",
        "flavor": "desired python flavor, one of: cpython (default), pypy, stackless",
        "tag": f"docker image tag (defaults to `{BASE_TAG}`)",
        "cache": "use docker cache when building (default)",
        "revision": "optional value for org.opencontainers.image.revision",
    },
)
def build_docker(
    ctx: Context,
    version: str,
    python_version: str,
    flavor: str = "cpython",
    tag: str = BASE_TAG,
    cache: bool = True,
    revision: str = "",
) -> None:
    """Build Docker image for the given Poetry version, using given Python as base."""
    # We have ordered composable dockerfiles, and we need to pick the corresponding ones according
    # to the flavor and version.
    # Additionally, the docker base python version may differ from the given desired version.
    # So we use an ordering for step 2 of the Dockerfiles, from 0 to 9, where the default sits at
    # 4; therefore, we can have actions before and/or after the standard step.
    # We could do the same for the other steps, but I don't want to over-engineer this even more,
    # given we don't need it for now. It would be straightforward to refactor later if that
    # becomes a need.
    flavor_to_base = {
        "cpython": Template("python:${version}-slim"),
        "pypy": Template("pypy:${version}-slim"),
        "stackless": Template("bcroq/stackless:${version}"),
    }
    flavor_version_to_docker_version = {
        "stackless": {
            "3.7": "3.7.8-slim-buster",
            "3.8": "3.8.1-slim-bullseye",
        },
    }
    potential_step2_dockerfiles = (
        "standard.Dockerfile",
        f"{flavor}.Dockerfile",
        f"{flavor}-{python_version}.Dockerfile",
    )

    if flavor not in flavor_to_base:
        raise Exit(
            f'flavor "{flavor}" is invalid, should be one of: {", ".join(flavor_to_base)}',
            code=1,
        )

    # Check if we need to change the version for the docker base image
    if version_to_docker_version := flavor_version_to_docker_version.get(flavor):
        try:
            docker_python_version = version_to_docker_version[python_version]
        except KeyError:
            raise Exit(
                f'invalid version for flavor "{flavor}", should be one of: '
                + f'{", ".join(version_to_docker_version)}',
                code=1,
            )
    else:
        docker_python_version = python_version

    base = flavor_to_base[flavor].substitute(version=docker_python_version)

    flavor_dockerfiles = sorted(
        filter(
            lambda file: any(
                file.endswith(potential) for potential in potential_step2_dockerfiles
            ),
            glob("2-*.Dockerfile"),
        ),
    )

    docker_files = (
        "0-base.Dockerfile",
        "1-debian.Dockerfile",
        *flavor_dockerfiles,
        "3-final.Dockerfile",
    )

    cache_arg = "" if cache else "--no-cache "

    full_tag = get_docker_tag(
        base_tag=tag,
        flavor=flavor,
        poetry_version=version,
        python_version=python_version,
    )

    ctx.run(
        f"cat {' '.join(docker_files)} | "
        + "sudo docker build "
        + "--compress "
        + "--pull "
        + "--rm "
        + f"{cache_arg}"
        + '--label "org.opencontainers.image.created"="$(date -Iseconds)" '
        + f'--label "org.opencontainers.image.revision"="{revision}" '
        + f"--tag {full_tag} "
        + f"--build-arg POETRY_VERSION={version} "
        + f"--build-arg BASE={base} -f - .",
        echo=True,
        pty=True,
        echo_stdin=False,
    )


@task
def build(ctx: Context, version: str, interactive: bool = False) -> None:
    """Build all supported docker images for the given Poetry version."""
    # Yeah, we kinda gonna have to update this every time a new Python version gets released...
    latest_cpython = 13
    latest_pypy = 10
    latest_stackless = 8

    flavors = {
        "cpython": tuple(f"3.{minor}" for minor in range(8, latest_cpython + 1))
        + (f"3.{latest_cpython + 1}-rc",),
        "pypy": tuple(f"3.{minor}" for minor in range(8, latest_pypy + 1)),
        "stackless": tuple(f"3.{minor}" for minor in range(8, latest_stackless + 1)),
    }

    for flavor, python_versions in flavors.items():
        if interactive:
            choice = input(
                f"Do you want to build image of Poetry v{version} for Python flavor {flavor}? "
                + "[Y/n] "
            )
            if choice.lower() == "n":
                continue
        for python_version in python_versions:
            if interactive:
                choice = input(
                    f"Do you want to build image of Poetry v{version} for Python "
                    + f"v{python_version}-{flavor}? [Y/n] "
                )
                if choice.lower() == "n":
                    continue

            build_docker(ctx, version, python_version=python_version, flavor=flavor)
