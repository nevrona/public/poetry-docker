COPY bin/rustup-init /usr/local/bin/rustup-init

RUN <<EOT
    set -eu

    apt-get update
    apt-get install -qy --no-install-recommends \
        libssl-dev \
        python3-dev \
        pkg-config

    apt-get autopurge
    apt-get clean
    rm -rf /var/lib/apt/lists/*
    rm -rf /var/cache/apt/*
EOT

RUN <<EOT
    set -eu

    # I don't particularly like blindly executing curl | sh as recommended by
    # cargo docs:
    # curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    # So let's use the binary
    chmod +x /usr/local/bin/rustup-init
    rustup-init -y --profile minimal
    . "$HOME/.cargo/env"
EOT

ENV PATH="/root/.cargo/bin:$PATH"
