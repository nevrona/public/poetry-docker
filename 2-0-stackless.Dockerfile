RUN <<EOT
    ln -s /opt/stackless/bin/python3 /bin/python
    ln -s /opt/stackless/bin/python3 /bin/python3
    ln -s /opt/stackless/bin/pip3 /bin/pip
    ln -s /opt/stackless/bin/pip3 /bin/pip3
EOT

ENV PATH="$PATH:/opt/stackless/bin"
