ARG BASE

FROM $BASE

LABEL maintainer="Nevrona FOSS <foss@nevrona.org>"
LABEL org.opencontainers.image.title="Poetry Docker Image"
LABEL org.opencontainers.image.description="Docker image for Poetry"
LABEL org.opencontainers.image.vendor="Nevrona S. A."
LABEL org.opencontainers.image.authors="HacKan <hackan@nevrona.org>"
LABEL org.opencontainers.image.version="0.6.1"
LABEL org.opencontainers.image.licenses="MPL-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/nevrona/public/poetry-docker"
LABEL org.opencontainers.image.url="https://gitlab.com/nevrona/public/poetry-docker"
LABEL org.opencontainers.image.created=""
LABEL org.opencontainers.image.revision=""
LABEL org.nevrona.url="https://nevrona.org"

ARG PYTHONUNBUFFERED=1
ARG POETRY_VIRTUALENVS_CREATE=0
ARG POETRY_VERSION
