RUN <<EOT
    set -eu

    apt-get update
    apt-get upgrade -qy --no-install-recommends

    apt-get install -qy --no-install-recommends \
        build-essential \
        libffi-dev \
        git

    apt-get autopurge
    apt-get clean
    rm -rf /var/lib/apt/lists/*
    rm -rf /var/cache/apt/*
EOT
