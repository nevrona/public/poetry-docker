RUN <<EOT
    set -eu

    pipx install "poetry==$POETRY_VERSION"
EOT
