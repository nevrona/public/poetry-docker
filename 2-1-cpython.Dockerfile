ENV PATH="$PATH:/root/.local/bin"

RUN <<EOT
    apt-get update

    apt-get install -qy --no-install-recommends \
        pipx

    apt-get autopurge
    apt-get clean
    rm -rf /var/lib/apt/lists/*
    rm -rf /var/cache/apt/*

    pipx ensurepath
EOT
