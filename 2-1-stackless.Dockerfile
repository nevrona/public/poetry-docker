ENV PATH="$PATH:/root/.local/bin"

RUN <<EOT
    python3 -m pip install pipx
    python3 -m pipx ensurepath
EOT
